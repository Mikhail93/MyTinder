package dao;

import userData.User;

import java.util.ArrayList;
import java.util.List;

public class UsersDAO {
  public static final List<User> users = new ArrayList<>();
  public static List<User> likedUsers = new ArrayList<>();
  public static List<User> skipedUsers = new ArrayList<>();
  public static String chatter;

  {
    users.add(new User("Jane", "https://i.imgur.com/N6SaAlZ.jpg"));
    users.add(new User("Lily", "http://www.cavemancircus.com/wp-content/uploads/images/2015/july/pretty_girls_5/pretty_girls_1.jpg"));
    users.add(new User("Mary", "https://data.whicdn.com/images/184290589/large.jpg"));
    users.add(new User("Camel", "https://i.imgur.com/gwf33zo.jpg"));
  }

  public User nextUser() {
    for (User user : users) {
      if (!likedUsers.contains(user) && !skipedUsers.contains(user)) {
        return user;
      }
    }
    return null;
  }

  public static User getChatter(List<User> likedUsers) {
    for (User user : likedUsers) {
      if (user.name.equals(chatter)) {
        return user;
      }
    }
    return null;
  }
}
