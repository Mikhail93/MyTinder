package filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@WebFilter(filterName = "cookieFilter", urlPatterns = "/*")
public class AuthFilter implements Filter {

  static Set<String> publicUrls = new HashSet<String>() {{
    add("/login");
  }};

  public static Map<String, String> cookiesMap = new HashMap<>();

  @Override
  public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
    HttpServletRequest req = (HttpServletRequest) servletRequest;
    HttpServletResponse resp = (HttpServletResponse) servletResponse;
    Cookie[] cookies = req.getCookies();
    boolean loggedIn = false;

    if (cookies != null) {
      for (Cookie cookie : cookies) {
        String name = cookie.getName();
        String value = cookie.getValue();
        System.out.printf("Cookie{ '%s' : '%s' }\n", name, value);

        if (name.equals("user-token") && cookiesMap.containsKey(value)) {
          loggedIn = true;
        }
      }
    }

    req.setAttribute("loggedIn", loggedIn);

    if (loggedIn || publicUrls.contains(req.getRequestURI())) {
      filterChain.doFilter(req, resp);
    } else {
      resp.sendRedirect("/login");
    }
  }

  @Override
  public void init (FilterConfig filterConfig) throws ServletException {

  }

  @Override
  public void destroy () {

  }
}
