package servlets;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import dao.UsersDAO;
import templateFile.TemplateWriteFile;
import userData.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

@WebServlet(urlPatterns = "/chats")
public class ChatServlet extends HttpServlet {
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    User chatter = UsersDAO.getChatter(UsersDAO.likedUsers);

    ArrayList<String> messages = Lists.newArrayList();
    TemplateWriteFile.write("chats.html",resp.getWriter(), ImmutableMap.of("message", messages, "user", chatter));
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
/*    Date date = new Date();
    SimpleDateFormat sdf = new SimpleDateFormat("HH:MM:SS");
    String time = sdf.format(date);*/

    User chatter = UsersDAO.getChatter(UsersDAO.likedUsers);
    String message = req.getParameter("message");
    chatter.messageHistory.add(message);
    resp.sendRedirect("/chats");
  }
}
