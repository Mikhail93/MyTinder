package servlets;

import dao.UsersDAO;
import filter.AuthFilter;
import templateFile.TemplateWriteFile;
import userData.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;

@WebServlet(loadOnStartup = 1, urlPatterns = "/*")
public class ChoseServlet extends HttpServlet {

  UsersDAO girls = new UsersDAO();

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    User thisUser = girls.nextUser();

    if (thisUser == null) {
      resp.sendRedirect("/chosenGirls");
      return;
    }

//    if ((boolean) req.getAttribute("loggedIn")) {
//      resp.sendRedirect("/");
//    }

    TemplateWriteFile.write("index.html", resp.getWriter(), thisUser);
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    String button = req.getParameter("button");

    User thisUser = girls.nextUser();

    if ("yes".equals(button)) {
      girls.likedUsers.add(thisUser);
      resp.sendRedirect("/");
    } else if ("no".equals(button)) {
      girls.skipedUsers.add(thisUser);
      resp.sendRedirect("/");
    }
  }
}