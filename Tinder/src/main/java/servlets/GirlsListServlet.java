package servlets;

import com.google.common.collect.ImmutableMap;
import dao.UsersDAO;
import templateFile.TemplateWriteFile;
import userData.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet(loadOnStartup = 1, urlPatterns = "/chosenGirls")
public class GirlsListServlet extends HttpServlet {

  List<User> likedGirls = UsersDAO.likedUsers;

  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, java.io.IOException {
    TemplateWriteFile.write("girlsLikedList.html",resp.getWriter(), ImmutableMap.of("girls", likedGirls));
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    String name = req.getParameter("openchat");
    UsersDAO.chatter = name;
    resp.sendRedirect("/chats");
  }
}
