package servlets;

import com.google.common.collect.ImmutableMap;
import filter.AuthFilter;
import templateFile.TemplateWriteFile;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@WebServlet(name = "login", loadOnStartup = 1, urlPatterns = "/*")
public class LoginServlet extends HttpServlet {

  public static Map<String, String> tokens = new HashMap<>();
  public static Map<String, String> knownUsers = new HashMap<String, String>() {{
    put("Michael", "ghbrjkbcn");
  }};

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    TemplateWriteFile.write("login.html", resp.getWriter(), knownUsers);
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    String userName = req.getParameter("userName");
    String password = req.getParameter("password");

    boolean known = knownUsers.containsKey(userName)
        && knownUsers.get(userName).equals(password);

    if (known) {
      String token = UUID.randomUUID().toString();
      tokens.put(token, userName);
      resp.addCookie(new Cookie("user-token", token));

      AuthFilter.cookiesMap.put(token, userName);

      resp.sendRedirect("/");
    } else {
      resp.sendRedirect("/login");
    }

  }
}
