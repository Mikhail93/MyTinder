package ua.danit;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import filter.AuthFilter;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import servlets.ChatServlet;
import servlets.ChoseServlet;
import servlets.GirlsListServlet;
import servlets.LoginServlet;

import javax.servlet.DispatcherType;

public class App {

    public static void main(String[] args) throws Exception {
    Server server = new Server(8080);

    ServletContextHandler handler = new ServletContextHandler();
    ServletHolder holder = new ServletHolder(new ChoseServlet());
    handler.addServlet(holder, "/*");

    GirlsListServlet girlsListServlet = new GirlsListServlet();
    handler.addServlet(new ServletHolder(girlsListServlet), "/chosenGirls");

    ChatServlet chatServlet = new ChatServlet();
    handler.addServlet(new ServletHolder(chatServlet), "/chats");

    LoginServlet loginServlet = new LoginServlet();
    handler.addServlet(new ServletHolder(loginServlet), "/login");

    AuthFilter authFilter = new AuthFilter();
    handler.addFilter(new FilterHolder(authFilter), "/*", Sets.newEnumSet(Lists.newArrayList(DispatcherType.REQUEST), DispatcherType.class));

    server.setHandler(handler);
    server.start();
    server.join();
  }
}